<?php

declare(strict_types=1);

namespace App\Forums\Infrastructure\Persistence;

use App\Core\Application\Search\SearchQuery;
use App\Core\Infrastructure\Search\Doctrine\Expression\CriteriaBuilder;
use App\Core\Infrastructure\Search\Doctrine\Expression\FuzzySearchBuilder;
use App\Core\Infrastructure\Search\Doctrine\Expression\Limitation;
use App\Core\Infrastructure\Search\Doctrine\Expression\Ordering;
use App\Forums\Domain\Topic;
use App\Forums\Domain\TopicReadStorage;
use App\Forums\Domain\TopicWriteStorage;
use Doctrine\DBAL\LockMode;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Query\QueryException;

final class TopicDoctrineRepository extends EntityRepository implements TopicReadStorage, TopicWriteStorage
{
    /**
     * @throws ORMException
     */
    public function add(Topic $topic): void
    {
        $this->getEntityManager()->persist($topic);
        $this->getEntityManager()->flush();
    }

    public function getAndLock(int $id): ?Topic
    {
        return $this->find($id, LockMode::PESSIMISTIC_WRITE);
    }

    public function get(int $id): ?Topic
    {
        return $this->find($id);
    }
    /**
     * @throws NonUniqueResultException
     * @throws QueryException
     */
    public function countBySearchQuery(SearchQuery $query): int
    {
        $count =
            $this->createQueryBuilder('t')
                ->select('COUNT(t.id) as count')
                ->addCriteria(FuzzySearchBuilder::search($query->getFuzzy(), ['t.title']))
                ->addCriteria(CriteriaBuilder::createByFilters($query->getFilters()))
                ->getQuery()
                ->getOneOrNullResult()
        ;

        return (int)$count['count'];
    }

    /**
     * @return Topic[]
     * @throws QueryException
     */
    public function getListBySearchQuery(SearchQuery $query): array
    {
        $queryBuilder =
            $this->createQueryBuilder('t')
                ->addCriteria(FuzzySearchBuilder::search($query->getFuzzy(), ['t.title']))
                ->addCriteria(CriteriaBuilder::createByFilters($query->getFilters()))
                ->addCriteria(
                    Limitation::limitation($query->getLimitation())
                )
            ;

        Ordering::add($queryBuilder, $query->getOrderQueries());

        return
            $queryBuilder
                ->getQuery()
                ->getResult()
            ;
    }

    /**
     * @throws ORMException
     */
    public function delete(Topic $topic): void
    {
        $this->getEntityManager()->remove($topic);
    }
}
