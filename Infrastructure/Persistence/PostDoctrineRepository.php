<?php

declare(strict_types=1);

namespace App\Forums\Infrastructure\Persistence;

use App\Core\Application\Search\SearchQuery;
use App\Core\Infrastructure\Search\Doctrine\Expression\CriteriaBuilder;
use App\Core\Infrastructure\Search\Doctrine\Expression\FuzzySearchBuilder;
use App\Core\Infrastructure\Search\Doctrine\Expression\Limitation;
use App\Core\Infrastructure\Search\Doctrine\Expression\Ordering;
use App\Forums\Domain\Post;
use App\Forums\Domain\PostReadStorage;
use App\Forums\Domain\PostWriteStorage;
use Doctrine\DBAL\LockMode;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Query\QueryException;

final class PostDoctrineRepository extends EntityRepository implements PostReadStorage, PostWriteStorage
{
    public function add(Post $post): void
    {
        $this->getEntityManager()->persist($post);
        $this->getEntityManager()->flush();
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     */
    public function delete(Post $post): void
    {
        $this->getEntityManager()->remove($post);
    }

    public function getAndLock(int $id): ?Post
    {
        return $this->find($id, LockMode::PESSIMISTIC_WRITE);
    }

    public function get(int $id): ?Post
    {
        return $this->find($id);
    }

    /**
     * @return Post[]
     * @throws QueryException
     */
    public function getListBySearchQuery(SearchQuery $query): array
    {
        $queryBuilder =
            $this->createQueryBuilder('t')
                ->addCriteria(FuzzySearchBuilder::search($query->getFuzzy(), ['t.text']))
                ->addCriteria(CriteriaBuilder::createByFilters($query->getFilters()))
                ->addCriteria(
                    Limitation::limitation($query->getLimitation())
                )
            ;

        Ordering::add($queryBuilder, $query->getOrderQueries());

        return
            $queryBuilder
                ->getQuery()
                ->getResult()
            ;
    }

    /**
     * @throws NonUniqueResultException
     * @throws QueryException
     */
    public function countBySearchQuery(SearchQuery $query): int
    {
        $count =
            $this->createQueryBuilder('t')
                ->select('COUNT(t.id) as count')
                ->addCriteria(FuzzySearchBuilder::search($query->getFuzzy(), ['t.text']))
                ->addCriteria(CriteriaBuilder::createByFilters($query->getFilters()))
                ->getQuery()
                ->getOneOrNullResult()
        ;

        return (int)$count['count'];
    }
}
