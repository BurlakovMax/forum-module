<?php

declare(strict_types=1);

namespace App\Forums\Domain;

use App\Forums\Application\CreatePostCommand;
use App\Forums\Application\PostNotFound;
use App\Forums\Application\TopicNotFound;
use App\Forums\Application\UpdatePostCommand;
use Exception;
use Symfony\Component\Messenger\MessageBusInterface;

final class PostService
{
    private PostWriteStorage $postWriteStorage;

    private TopicWriteStorage $topicWriteStorage;

    private AccountStorage $accountStorage;

    private MessageBusInterface $bus;

    public function __construct(
        PostWriteStorage $postWriteStorage,
        TopicWriteStorage $topicWriteStorage,
        AccountStorage $accountStorage,
        MessageBusInterface $bus
    ) {
        $this->postWriteStorage = $postWriteStorage;
        $this->topicWriteStorage = $topicWriteStorage;
        $this->accountStorage = $accountStorage;
        $this->bus = $bus;
    }

    /**
     * @throws TopicNotFound
     * @throws Exception
     */
    public function create(CreatePostCommand $command): int
    {
        $topic = $this->getTopic($command->getTopicId());

        $post = new Post(
            $command->getTopicId(),
            $command->getAccountId(),
            $topic->getLocationId(),
            $topic->getForumId(),
            $command->getText()
        );
        $this->postWriteStorage->add($post);

        $topic
            ->updateLastPost(
                $post->getId(),
                $post->getAccountId(),
                $post->getCreatedAt()
            )
        ;

        $this->bus->dispatch(
            new PostCreated(
                $topic->getId(),
                $command->getAccountId()
            )
        );

        return $post->getId();
    }

    /**
     * @throws TopicNotFound
     */
    private function getTopic(int $topicId): Topic
    {
        $topic = $this->topicWriteStorage->getAndLock($topicId);

        if (null === $topic) {
            throw new TopicNotFound();
        }

        return $topic;
    }

    /**
     * @throws PostNotFound
     */
    public function update(UpdatePostCommand $command): void
    {
        $this
            ->getPost(
                $command->getId()
            )
            ->update(
                $this->accountStorage->get($command->getAccountId())->getName(),
                $command->getReason(),
                $command->getText()
            )
        ;
    }

    public function delete(int $id): void
    {
        $post = $this->getPost($id);

        $this->postWriteStorage->delete($post);
    }

    /**
     * @throws PostNotFound
     */
    private function getPost(int $postId): Post
    {
        $post = $this->postWriteStorage->getAndLock($postId);

        if (null === $post) {
            throw new PostNotFound();
        }

        return $post;
    }
}
