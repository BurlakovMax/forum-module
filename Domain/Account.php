<?php

declare(strict_types=1);

namespace App\Forums\Domain;

use LazyLemurs\Structures\Email;

final class Account
{
    private int $id;

    private string $name;

    private Email $email;

    public function __construct(int $id, string $name, Email $email)
    {
        $this->id = $id;
        $this->name = $name;
        $this->email = $email;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getEmail(): Email
    {
        return $this->email;
    }
}
