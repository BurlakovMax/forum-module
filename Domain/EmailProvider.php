<?php

declare(strict_types=1);

namespace App\Forums\Domain;

use App\EmailGate\Domain\EmailData;
use Throwable;

interface EmailProvider
{
    /**
     * @throws Throwable
     */
    public function sendSync(EmailData $emailData): void;
}
