<?php

declare(strict_types=1);

namespace App\Forums\Domain;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Query\QueryException;

final class SubscriberService
{
    private SubscriberWriteStorage $subscriberWriteStorage;

    private SubscriberReadStorage $subscriberReadStorage;

    public function __construct(
        SubscriberWriteStorage $subscriberWriteStorage,
        SubscriberReadStorage $subscriberReadStorage
    ) {
        $this->subscriberWriteStorage = $subscriberWriteStorage;
        $this->subscriberReadStorage = $subscriberReadStorage;
    }

    /**
     * @throws NonUniqueResultException
     * @throws ORMException
     * @throws QueryException
     */
    public function subscribe(int $topicId, int $accountId): int
    {
        $subscriber = $this->subscriberReadStorage->getByTopicIdAndAccountId($topicId, $accountId);

        if ($subscriber !== null) {
            return $subscriber->getId();
        }

        return $this->create($topicId, $accountId);
    }

    /**
     * @throws NonUniqueResultException
     * @throws ORMException
     * @throws QueryException
     */
    public function unsubscribe(int $topicId, int $accountId): void
    {
        $subscriber = $this->subscriberReadStorage->getByTopicIdAndAccountId($topicId, $accountId);

        if (isset($subscriber)) {
            $this->subscriberWriteStorage->remove($subscriber);
        }
    }

    /**
     * @throws ORMException
     */
    private function create(int $topicId, int $accountId): int
    {
        $subscriber = new Subscriber($topicId, $accountId);

        $this->subscriberWriteStorage->add($subscriber);

        return $subscriber->getId();
    }
}
