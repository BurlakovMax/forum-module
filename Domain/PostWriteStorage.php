<?php

declare(strict_types=1);

namespace App\Forums\Domain;

use Doctrine\ORM\ORMException;

interface PostWriteStorage
{
    /**
     * @throws ORMException
     */
    public function add(Post $post): void;

    public function getAndLock(int $id): ?Post;

    public function delete(Post $post): void;
}
