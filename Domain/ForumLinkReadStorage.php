<?php

declare(strict_types=1);

namespace App\Forums\Domain;

interface ForumLinkReadStorage
{
    public function get(int $forumId): ?ForumLink;

    public function getByUrl(string $url): ?ForumLink;

    /**
     * @param int[] $ids
     * @return ForumLink[]
     */
    public function getByIds(array $ids): array;
}