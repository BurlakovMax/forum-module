<?php

declare(strict_types=1);

namespace App\Forums\Domain;

use LazyLemurs\DomainEvents\DomainEvent;

abstract class Event extends DomainEvent
{
    private int $topicId;

    private int $accountId;

    public function __construct(int $topicId, int $accountId)
    {
        $this->topicId = $topicId;
        $this->accountId = $accountId;

        parent::__construct();
    }

    public function getTopicId(): int
    {
        return $this->topicId;
    }

    public function getAccountId(): int
    {
        return $this->accountId;
    }
}
