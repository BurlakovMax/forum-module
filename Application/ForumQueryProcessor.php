<?php

declare(strict_types=1);

namespace App\Forums\Application;

use App\Forums\Domain\Forum;
use App\Forums\Domain\ForumReadStorage;

final class ForumQueryProcessor
{
    private ForumReadStorage $forumReadStorage;

    public function __construct(ForumReadStorage $forumReadStorage)
    {
        $this->forumReadStorage = $forumReadStorage;
    }

    /**
     * @throws ForumNotFound
     */
    public function getByIdAndLocationId(int $id, int $locationId): ForumData
    {
        $forum = $this->forumReadStorage->getByIdAndLocationId($id, $locationId);

        if (null === $forum) {
            throw new ForumNotFound();
        }

        return $this->mapToData($forum);
    }

    /**
     * @return ForumData[]
     */
    public function getByLocationId(int $locationId): array
    {
        return array_map([ $this, 'mapToData' ], $this->forumReadStorage->getByLocationId($locationId));
    }

    private function mapToData(Forum $forum): ForumData
    {
        return new ForumData($forum);
    }
}
