<?php

declare(strict_types=1);

namespace App\Forums\Application;

use App\Forums\Domain\Subscriber;
use App\Forums\Domain\SubscriberReadStorage;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Query\QueryException;

final class SubscriberQueryProcessor
{
    private SubscriberReadStorage $subscriberReadStorage;

    public function __construct(SubscriberReadStorage $subscriberReadStorage)
    {
        $this->subscriberReadStorage = $subscriberReadStorage;
    }

    /**
     * @throws SubscriberNotFound
     * @throws NonUniqueResultException
     * @throws QueryException
     */
    public function getByTopicIdAndAccountId(int $topicId, int $accountId): SubscriberData
    {
        $subscriber = $this->subscriberReadStorage->getByTopicIdAndAccountId($topicId, $accountId);

        if ($subscriber === null) {
            throw new SubscriberNotFound();
        }

        return $this->mapToData($subscriber);
    }

    /**
     * @return SubscriberData[]
     */
    public function getByTopicId(int $topicId): array
    {
        return $this->mapListToData($this->subscriberReadStorage->getByTopicId($topicId));
    }

    private function mapToData(Subscriber $subscriber): SubscriberData
    {
        return new SubscriberData($subscriber);
    }

    /**
     * @param Subscriber[]
     * @return SubscriberData[]
     */
    private function mapListToData(array $subscribers): array
    {
        return array_map(fn($subscriber): SubscriberData => new SubscriberData($subscriber), $subscribers);
    }
}
