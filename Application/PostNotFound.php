<?php

declare(strict_types=1);

namespace App\Forums\Application;

use LazyLemurs\Exceptions\NotFoundException;

final class PostNotFound extends NotFoundException
{

}
