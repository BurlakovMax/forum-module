<?php

declare(strict_types=1);

namespace App\Forums\Application;

use LazyLemurs\Commander\Property;

final class UpdatePostCommand extends PostCommand
{
    private int $id;

    /**
     * @Property(type="string")
     */
    private string $reason;

    public function __construct(int $id, int $accountId)
    {
        $this->id = $id;
        parent::__construct($accountId);
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getReason(): string
    {
        return $this->reason;
    }
}
