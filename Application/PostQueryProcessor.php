<?php

declare(strict_types=1);

namespace App\Forums\Application;

use App\Core\Application\Search\SearchQuery;
use App\Forums\Domain\Account;
use App\Forums\Domain\AccountStorage;
use App\Forums\Domain\Post;
use App\Forums\Domain\PostReadStorage;

final class PostQueryProcessor
{
    private PostReadStorage $postReadStorage;

    private AccountStorage $accountStorage;

    public function __construct(PostReadStorage $postReadStorage, AccountStorage $accountStorage)
    {
        $this->postReadStorage = $postReadStorage;
        $this->accountStorage = $accountStorage;
    }

    /**
     * @throws PostNotFound
     */
    public function get(int $id): PostData
    {
        $post = $this->postReadStorage->get($id);

        if (null === $post) {
            throw new PostNotFound();
        }

        $account = $this->accountStorage->get($post->getAccountId());

        return $this->mapToData($post, $account);
    }

    public function countBySearchQuery(SearchQuery $query): int
    {
        return $this->postReadStorage->countBySearchQuery($query);
    }

    /**
     * @return PostData[]
     */
    public function getListBySearchQuery(SearchQuery $query): array
    {
        $rawPosts = $this->postReadStorage->getListBySearchQuery($query);

        $posts = [];
        foreach ($rawPosts as $post) {
            $posts[] = $this->mapToData($post, $this->accountStorage->get($post->getAccountId()));
        }

        return $posts;
    }

    private function mapToData(Post $post, Account $account): PostData
    {
        return new PostData($post, $account);
    }
}
