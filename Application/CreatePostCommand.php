<?php

declare(strict_types=1);

namespace App\Forums\Application;

use LazyLemurs\Commander\Property;

final class CreatePostCommand extends PostCommand
{
    /**
     * @Property()
     */
    private int $topicId;

    public function getTopicId(): int
    {
        return $this->topicId;
    }
}
