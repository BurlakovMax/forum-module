<?php

declare(strict_types=1);

namespace App\Forums\Application;

use App\Forums\Domain\Account;
use App\Forums\Domain\Topic;
use DateTimeImmutable;
use LazyLemurs\Structures\Email;
use Swagger\Annotations as SWG;

final class TopicData
{
    /**
     * @SWG\Property()
     */
    private int $id;

    /**
     * @SWG\Property()
     */
    private ?string $title;

    /**
     * @SWG\Property()
     */
    private ?int $placeId;

    /**
     * @SWG\Property()
     */
    private int $accountId;

    /**
     * @SWG\Property()
     */
    private string $accountName;

    /**
     * @SWG\Property()
     */
    private Email $accountEmail;

    /**
     * @SWG\Property()
     */
    private DateTimeImmutable $createdAt;

    /**
     * @SWG\Property()
     */
    private int $countReplies;

    /**
     * @SWG\Property()
     */
    private int $lastPostId;

    /**
     * @SWG\Property()
     */
    private ?int $lastPostAccountId;

    /**
     * @SWG\Property()
     */
    private ?int $forumId;

    /**
     * @SWG\Property()
     */
    private int $locationId;

    /**
     * @SWG\Property()
     */
    private ?DateTimeImmutable $lastPostCreatedAt;

    /**
     * @SWG\Property()
     */
    private ?bool $important;

    public function __construct(Topic $topic, Account $account)
    {
        $this->id = $topic->getId();
        $this->title = $topic->getTitle();
        $this->placeId = $topic->getPlaceId();
        $this->accountId = $topic->getAccountId();
        $this->accountName = $account->getName();
        $this->accountEmail = $account->getEmail();
        $this->createdAt = $topic->getCreatedAt();
        $this->countReplies = $topic->getCountReplies();
        $this->lastPostId = $topic->getLastPostId();
        $this->lastPostAccountId = $topic->getLastPostAccountId();
        $this->forumId = $topic->getForumId();
        $this->locationId = $topic->getLocationId();
        $this->lastPostCreatedAt = $topic->getLastPostCreatedAt();
        $this->important = $topic->getImportant();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function getPlaceId(): ?int
    {
        return $this->placeId;
    }

    public function getAccountId(): int
    {
        return $this->accountId;
    }

    public function getAccountName(): string
    {
        return $this->accountName;
    }

    public function getAccountEmail(): Email
    {
        return $this->accountEmail;
    }

    public function getCreatedAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getCountReplies(): int
    {
        return $this->countReplies;
    }

    public function getLastPostId(): int
    {
        return $this->lastPostId;
    }

    public function getLastPostAccountId(): ?int
    {
        return $this->lastPostAccountId;
    }

    public function getForumId(): ?int
    {
        return $this->forumId;
    }

    public function getLocationId(): int
    {
        return $this->locationId;
    }

    public function getLastPostCreatedAt(): ?DateTimeImmutable
    {
        return $this->lastPostCreatedAt;
    }

    public function getImportant(): ?bool
    {
        return $this->important;
    }
}
